using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UIElements;
using TMPro;
using wssrv;

public class GgClient : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    int count = 0;
    public GameObject msgs, content;
    public void SendTestMessage()
    {
        string sender = "unity";
        // string content = $"test message from unity {count++}";
        var content = this.content.GetComponent<TMP_InputField>().text;
        if (content == "")
        {
            content = $"test message from unity {count}";
        }
        count++;
        var httpClient = new HttpClient();
        var query = $"?sender={sender}&content={content}";
        var url = "http://localhost:4649/Message" + query;
        // var message = new Message
        // {
        //     Sender = uid,
        //     Content = content
        // };
        // var json = JsonConvert.SerializeObject(message);
        // var contentToSend = new StringContent(json);
        var response = httpClient.PostAsync(url, null).Result;
        var responseContent = response.Content.ReadAsStringAsync().Result;
        Debug.Log(responseContent);
    }

    public void GetAllMessages()
    {
        // call http api to get all messages
        // host set to localhost:4649
        var httpClient = new HttpClient();
        var response = httpClient.GetAsync("http://localhost:4649/Message").Result;
        var content = response.Content.ReadAsStringAsync().Result;
        Debug.Log(content);
        var messages = JsonConvert.DeserializeObject<List<Message>>(content);
        var lines = new List<string>();
        foreach (var message in messages)
        {
            var s = $"{message.Sender}: {message.Content}";
            lines.Add(s);
            Debug.Log(s);
        }
        msgs.GetComponent<TextMeshProUGUI>().text = string.Join("\n", lines);
    }
}
